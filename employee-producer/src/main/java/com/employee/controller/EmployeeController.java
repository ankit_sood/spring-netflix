package com.employee.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.model.Employee;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@GetMapping("/{id}")
	public Employee getEmployee(@PathVariable("id")String id) {
		Employee emp = new Employee();
		emp.setEmpId(id);
		emp.setDesignation("Manager");
		emp.setName("Employee");
		emp.setSalary(15000D);
		return emp;
	}
}
